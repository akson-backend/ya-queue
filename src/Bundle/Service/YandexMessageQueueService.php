<?php

namespace Akson\Bundle\SQSQueue\Service;

use Akson\Bundle\SQSQueue\Interfaces\MessageQueueServiceInterface;
use Akson\Bundle\SQSQueue\Model\IncomingQueueMessage;
use Akson\Bundle\SQSQueue\Model\OutgoingQueueMessage;
use Aws\Sqs\SqsClient;
use Exception;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\SerializerInterface;

class YandexMessageQueueService implements MessageQueueServiceInterface
{
    private LoggerInterface $logger;
    private SqsClient $client;
    private string $queueUrl;
    private SerializerInterface $serializer;

    /**
     * YandexMessageQueueService constructor.
     * @param LoggerInterface $logger
     * @param SerializerInterface $serializer
     */
    public function __construct(LoggerInterface $logger, SerializerInterface $serializer)
    {
        $this->logger = $logger;
        $this->queueUrl = $_ENV["QUEUE_NAME"] ?? "";
        $this->client = new SqsClient([
            "version" => $_ENV["VERSION"] ?? "latest",
            "region" => $_ENV["REGION"] ?? "ru-central1",
            "endpoint" => $_ENV["ENDPOINT"] ?? "https://message-queue.api.cloud.yandex.net"
        ]);
        $this->serializer = $serializer;
    }

    public function publishMessage(OutgoingQueueMessage $message): ?string
    {
        $result = $this->client->sendMessage([
            'QueueUrl' => $this->queueUrl,
            'MessageBody' => $message->getMessage(),
            'DelaySeconds' => $message->getDelaySeconds()
        ]);

        return $result->get("MessageId");
    }

    public function receiveMessages(int $count, int $waitInSeconds = 20): array
    {
        $messages = [];

        $response = $this->client->receiveMessage([
            "QueueUrl" => $this->queueUrl,
            "MaxNumberOfMessages" => $count,
            "WaitTimeSeconds" => $waitInSeconds
        ]);

        if (isset($response->toArray()["Messages"])) {
            foreach ($response->toArray()["Messages"] as $message) {
                try {
                    $json = json_encode($message, JSON_THROW_ON_ERROR);
                    $incomingMessage = $this->serializer->deserialize($json, IncomingQueueMessage::class, "json");
                    $messages[] = $incomingMessage;
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), $message);
                }
            }
        }

        return $messages;
    }

    public function deleteMessage(IncomingQueueMessage $message): bool
    {
        $result = false;

        try {
            $response = $this->client->deleteMessage([
                "QueueUrl" => $this->queueUrl,
                "ReceiptHandle" => $message->getReceiptHandle()
            ]);

            if (isset($response->get("@metadata")["statusCode"]) && (int)$response->get("@metadata")["statusCode"] === 200) {
                $result = true;
            }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $result;
    }

    public function publishMessageBatch(array $messages): int
    {
        $result = 0;

        $request = [
            "QueueUrl" => $this->queueUrl,
            "Entries" => []
        ];

        try {
            foreach ($messages as $message) {
                if ($message instanceof OutgoingQueueMessage) {
                    $uuid = Uuid::uuid4()->toString();
                    $data = [
                        "Id" => $uuid,
                        "MessageBody" => $message->getMessage(),
                        "DelaySeconds" => $message->getDelaySeconds(),
//                        "MessageAttributes" => [
//                            "uuid" => [
//                                "DataType" => "application/json"
//                            ]
//                        ]
                    ];
                    $request["Entries"][] = $data;
                }
            }
            if (count($request["Entries"]) > 0) {
                $sended = $this->client->sendMessageBatch($request);
                if ($sended->get("Successful")) {
                    $result = count($sended->get("Successful"));
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $result;
    }
}