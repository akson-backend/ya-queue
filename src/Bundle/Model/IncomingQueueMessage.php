<?php

namespace Akson\Bundle\SQSQueue\Model;

use Symfony\Component\Validator\Constraints as Assert;

class IncomingQueueMessage
{
    /**
     * @Assert\Type(type="string")
     * @var string
     */
    private string $MessageId;
    /**
     * @Assert\Type(type="string")
     * @var string
     */
    private string $ReceiptHandle;
    /**
     * @Assert\Type(type="string")
     * @var string
     */
    private string $MD5OfBody;
    /**
     * @Assert\Type(type="string")
     * @var string
     */
    private string $Body;
    /**
     * @Assert\Type(type="array")
     * @var array
     */
    private array $Attributes;

    /**
     * @return string
     */
    public function getMessageId(): string
    {
        return $this->MessageId;
    }

    /**
     * @param string $MessageId
     */
    public function setMessageId(string $MessageId): void
    {
        $this->MessageId = $MessageId;
    }

    /**
     * @return string
     */
    public function getReceiptHandle(): string
    {
        return $this->ReceiptHandle;
    }

    /**
     * @param string $ReceiptHandle
     */
    public function setReceiptHandle(string $ReceiptHandle): void
    {
        $this->ReceiptHandle = $ReceiptHandle;
    }

    /**
     * @return string
     */
    public function getMD5OfBody(): string
    {
        return $this->MD5OfBody;
    }

    /**
     * @param string $MD5OfBody
     */
    public function setMD5OfBody(string $MD5OfBody): void
    {
        $this->MD5OfBody = $MD5OfBody;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->Body;
    }

    /**
     * @param string $Body
     */
    public function setBody(string $Body): void
    {
        $this->Body = $Body;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->Attributes;
    }

    /**
     * @param array $Attributes
     */
    public function setAttributes(array $Attributes): void
    {
        $this->Attributes = $Attributes;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "{MessageId:" . $this->MessageId
            . ", ReceiptHandle: " . $this->ReceiptHandle
            . ", MD5OfBody: " . $this->MD5OfBody
            . ", Body: " . $this->Body
            . ", Attributes: [ " . implode(",", $this->Attributes) . "]}";
    }
}