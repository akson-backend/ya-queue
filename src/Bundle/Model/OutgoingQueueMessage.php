<?php

namespace Akson\Bundle\SQSQueue\Model;

class OutgoingQueueMessage
{
    private string $message;
    private string $queue;
    private int $delaySeconds;

    /**
     * QueueMessage constructor.
     */
    public function __construct()
    {
        $this->queue = $_ENV["QUEUE_NAME"];
        $this->delaySeconds = 0;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getQueue(): string
    {
        return $this->queue;
    }

    /**
     * @param string $queue
     */
    public function setQueue(string $queue): void
    {
        $this->queue = $queue;
    }

    /**
     * @return int
     */
    public function getDelaySeconds(): int
    {
        return $this->delaySeconds;
    }

    /**
     * @param int $delaySeconds
     */
    public function setDelaySeconds(int $delaySeconds): void
    {
        $this->delaySeconds = $delaySeconds;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "{message: " . $this->message
            . ", queue: " . $this->queue
            . ", delaySeconds: " . $this->delaySeconds . "}";
    }
}