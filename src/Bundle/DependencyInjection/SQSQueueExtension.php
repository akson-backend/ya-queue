<?php

namespace Akson\Bundle\SQSQueue\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class SQSQueueExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $xmlFileLoader = new XmlFileLoader($container, new FileLocator(__DIR__ . "/../Resources/config"));
        $xmlFileLoader->load("services.xml");
    }
}